from utils.gorest_handler import GoRESTHandler
from faker import Faker

gorest_handler = GoRESTHandler()

# user data
user_data = {
    "name": "Stefan Banach",
    "email": "Stefan.Banach" + Faker().email(),
    "gender": "male",
    "status": "active"
}

user_updated_data = {
    "name": Faker().name(),
    "email": Faker().email(),
    "gender": "male",
    "status": "inactive"
}


def test_all_possible_activities_on_user():

    # Utwórz nowego użytkownika (biblioteka “faker? :)
    body = gorest_handler.create_user(user_data).json()
    user_id = body["id"]
    assert "id" in body
    assert body["email"] == user_data["email"]
    assert body["name"] == user_data["name"]

    # Zweryfikuj poprawność danych (name & email)
    body = gorest_handler.get_user(user_id).json()
    assert body["name"] == user_data["name"]
    assert body["email"] == user_data["email"]

    # Pozostałe dane
    assert body["status"] == user_data["status"]
    assert body["gender"] == user_data["gender"]

    # Wykonaj update użytkownika
    body = gorest_handler.update_user(user_id, user_updated_data).json()
    assert body["name"] != user_data["name"]
    assert body["gender"] == user_data["gender"]
    assert body["email"] != user_data["email"]
    assert body["status"] != user_data["status"]

    # Usuń użytkownika
    gorest_handler.delete_user(user_id)
